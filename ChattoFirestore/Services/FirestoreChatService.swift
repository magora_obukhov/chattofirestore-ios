import Foundation
import Firebase
import RxSwift

protocol ChatService {
    func create(roomNamed roomName: String, owner: User) -> Single<ChatRoom>
    func roomNamed(_ nameToFound: String) -> Single<ChatRoom?>
    func allRooms() -> Single<[ChatRoom]>
    func allRoomsObserver() -> Observable<[ChatRoom]>
    func messageObserver(roomID: String, startMessage: ChatMessage?, countLimit: UInt?) -> Observable<ChatMessage>
    func messages(roomID: String, startMessage: ChatMessage?, countLimit: UInt?) -> Single<[ChatMessage]>
    func create(message: String, chatRoom: ChatRoom, user: User) -> Single<ChatMessage>
}


class FirestoreService: ChatService {
    private var db: Firestore
    
    init() {
        self.db = Firestore.firestore()
    }
    
    //MARK: - Rooms
    
    func create(roomNamed roomName: String, owner: User) -> Single<ChatRoom> {
        return self.roomNamed(roomName)
            .flatMap({ (roomFound) in
                guard roomFound == nil else { return Single.just(roomFound!)}
                
                return Single<ChatRoom>.create(subscribe: { (single) -> Disposable in
                    let ref = self.db.collection("rooms").document()
                    let roomToCreate = ChatRoomDTO(id: "", name: roomName, owner: owner.uid)
                    ref.setData(roomToCreate.toJSON(),
                                completion: { (error) in
                                    if let error = error {
                                        print(error)
                                        single(.error(error))
                                    } else {
                                        var chatRoom = ChatRoom(dto: roomToCreate)
                                        chatRoom.ref = ref
                                        single(.success(chatRoom))
                                    }
                    })
                    return Disposables.create()
                })
            })
    }
    
    func roomNamed(_ nameToFound: String) -> Single<ChatRoom?> {
        return Single<ChatRoom?>.create(subscribe: { [weak self] (single) -> Disposable in
            let ref = self?.db.collection("rooms").whereField("name", isEqualTo: nameToFound)
            ref?.getDocuments { (snapshot, error) in
                if let error = error {
                    print(error)
                    single(.error(error))
                } else {
                    if let firstRoom = snapshot!.documents.first,
                        var roomFound = ChatRoomDTO(JSON: firstRoom.data()) {
                        roomFound.id = firstRoom.documentID
                        return single(.success(ChatRoom(dto: roomFound)))
                    }
                }
                single(.success(nil))
            }
            return Disposables.create()
        })
    }
    
    func allRooms() -> Single<[ChatRoom]> {
        return Single<[ChatRoom]>.create(subscribe: {[weak self] (single) -> Disposable in
            self?.db.collection("rooms").getDocuments(completion: { (snapshot, error) in
                if let error = error {
                    print(error)
                    single(.error(error))
                } else {
                    let rooms = snapshot!.documents
                        .compactMap{ (querySnapshot) -> ChatRoomDTO? in
                            var chatRoomDTO = ChatRoomDTO(JSON: querySnapshot.data())
                            chatRoomDTO?.id = querySnapshot.documentID
                            return chatRoomDTO
                        }
                        .compactMap{ChatRoom(dto: $0)}
                    single(.success(rooms))
                }
            })
            return Disposables.create()
        })
    }
    
    func allRoomsObserver() -> Observable<[ChatRoom]> {
        return Observable<[ChatRoom]>.create({[weak self] (subscriber) -> Disposable in
            let listener = self?.db.collection("rooms").addSnapshotListener({ (snapshot, error) in
                guard let snapshot = snapshot else {
                    print(error!)
                    subscriber.onError(error!)
                    return
                }
                let rooms = snapshot.documents
                    .compactMap{ (querySnapshot) -> ChatRoomDTO? in
                        var chatRoomDTO = ChatRoomDTO(JSON: querySnapshot.data())
                        chatRoomDTO?.id = querySnapshot.documentID
                        return chatRoomDTO
                    }
                    .compactMap{ChatRoom(dto: $0)}
                subscriber.onNext(rooms)
            })
            return Disposables.create {
                listener?.remove()
            }
        })
    }
    
    
    func messageObserver(roomID: String, startMessage: ChatMessage?, countLimit: UInt?) -> Observable<ChatMessage> {
        return Observable<ChatMessage>.create({ (subscriber) -> Disposable in
            var query = self.db.collection("rooms")
                .document(roomID)
                .collection("messages")
                .order(by: "timeStamp")
                .limit(to: Int(countLimit ?? 0))
            
            if let startSnapshot = startMessage?.documentSnapshot {
                query = query.start(atDocument: startSnapshot)
            }
            
            let listener = query.addSnapshotListener({ (querySnapshot, error) in
                guard let querySnapshot = querySnapshot else {
                    print(error!)
                    subscriber.onError(error!)
                    return
                }
                let messages = querySnapshot.documents.compactMap({ (documentSnapshot) -> ChatMessage? in
                    guard let chatMessageDTO = ChatMessageDTO(JSON: documentSnapshot.data()) else {
                        return nil
                    }
                    
                    var chatMessage = ChatMessage(dto: chatMessageDTO)
                    chatMessage.documentSnapshot = documentSnapshot
                    chatMessage.id = documentSnapshot.documentID
                    return chatMessage
                })
                
                //It's possible to return the entire array, but in order to comply the RTDB protocol
                //we send it one by one
                for message in messages {
                    subscriber.onNext(message)
                }
            })
            
            return Disposables.create {
                listener.remove()
            }
        })
    }
    
    func messages(roomID: String, startMessage: ChatMessage?, countLimit: UInt?) -> Single<[ChatMessage]> {
        return Single<[ChatMessage]>.create(subscribe: { (single) -> Disposable in
            var query = self.db.collection("rooms")
                .document(roomID)
                .collection("messages")
                .order(by: "timeStamp")
                .limit(to: Int(countLimit ?? 0))
            
            if let startSnapshot = startMessage?.documentSnapshot {
                query = query.start(atDocument: startSnapshot)
            }
            
            query.getDocuments(completion: { (querySnapshot, error) in
                guard let querySnapshot = querySnapshot else {
                    print(error!)
                    single(.error(error!))
                    return
                }
                let messages = querySnapshot.documents
                    .compactMap({ (documentSnapshot) -> ChatMessage? in
                        guard let chatMessageDTO = ChatMessageDTO(JSON: documentSnapshot.data()) else {
                            return nil
                        }
                        
                        var chatMessage = ChatMessage(dto: chatMessageDTO)
                        chatMessage.documentSnapshot = documentSnapshot
                        return chatMessage
                    })
                single(.success(messages))
                
            })
            return Disposables.create()
        })
    }
    
    func create(message: String, chatRoom: ChatRoom, user: User) -> Single<ChatMessage> {
        return Single<ChatMessage>.create(subscribe: { (single) -> Disposable in
            let ref = self.db.collection("rooms").document(chatRoom.id).collection("messages").document()
            let messageToCreate = ChatMessageDTO(senderID: user.uid, senderName: user.displayName, text: message)
            
            ref.setData(messageToCreate.toJSON(),
                        completion: { (error) in
                            if let error = error {
                                print(error)
                                single(.error(error))
                            } else {
                                var message = ChatMessage(dto: messageToCreate)
                                message.id = ref.documentID
                                single(.success(message))
                            }
            })
            
            return Disposables.create()
        })
    }
}

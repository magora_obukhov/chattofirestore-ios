import Foundation
import RxSwift
import FirebaseAuth
import FirebaseFirestore

protocol FBAuthService {
    var isAuthorized: Bool { get }
    var user: User? { get }
    func signUp(email: String, password: String, nickname: String) -> Completable
    func signIn(email: String, password: String) -> Single<User>
}

class FBAuthServiceImpl: FBAuthService {
    
    var isAuthorized: Bool { return Auth.auth().currentUser != nil }
    var user: User? { return Auth.auth().currentUser }
    
    func signUp(email: String, password: String, nickname: String) -> Completable {
        return Completable.create(subscribe: { (completable) -> Disposable in
            Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                if let error = error {
                    completable(.error(error))
                    return
                }
                let ref = Firestore.firestore().collection("users").document(user!.uid)
                ref.setData(["email": email, "nickname": nickname], completion: { (error) in
                    if let error = error {
                        completable(.error(error))
                    }
                    completable(.completed)
                })

                let changeRequest = user!.createProfileChangeRequest()
                changeRequest.displayName = nickname
                changeRequest.commitChanges(completion: nil)

            }
            return Disposables.create()
        })
    }
    
    func signIn(email: String, password: String) -> Single<User> {
        return Single<User>.create(subscribe: { (single) -> Disposable in
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if let error = error {
                    single(.error(error))
                    return
                }
                single(.success(user!))
            }
            return Disposables.create()
        })
    }

}

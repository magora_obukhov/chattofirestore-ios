import UIKit

protocol AlertDisplayingService {
    func showLocalizedError(_ error: Error, okHandler: (()->())?)
    func showOkCancelMessage(title: String?, message: String?, okHandler: (()->())?, cancelHandler: (()->())?)
    func showOkCancelInputAlert(title: String?, message: String?, okHandler: ((String?)->())?, cancelHandler: (()->())?)
}

class AlertControllerService: AlertDisplayingService {
    
    func showLocalizedError(_ error: Error,  okHandler: (()->())? = nil) {
        self.showMessage(title: "Error", message: error.localizedDescription, hasOk: true, hasCancel: false, hasTextField: false, presentFrom: nil, okHandler: {_ in okHandler?() })
    }
    
    func showOkCancelMessage(title: String?, message: String?, okHandler: (()->())?, cancelHandler: (()->())?) {
        self.showMessage(title: title, message: message, hasOk: true, hasCancel: true, hasTextField: false, presentFrom: nil, okHandler: {_ in okHandler?() }, cancelHandler: cancelHandler)
    }

    func showOkCancelInputAlert(title: String?, message: String?, okHandler: ((String?)->())?, cancelHandler: (()->())?) {
        self.showMessage(title: title, message: message, hasOk: true, hasCancel: true, hasTextField: true, presentFrom: nil, okHandler: okHandler, cancelHandler: cancelHandler)
    }
    
    private func showMessage(title: String?, message: String?, hasOk: Bool, hasCancel: Bool, hasTextField: Bool, presentFrom controller: UIViewController?,
                     okHandler: ((String?) -> ())?, cancelHandler: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if hasOk {
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                var textFieldValue: String?
                if let textFields = alert.textFields, textFields.count > 0 {
                    textFieldValue = textFields[0].text
                }
                okHandler?(textFieldValue)
            }
            alert.addAction(okAction)
        }
        
        if hasCancel {
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                cancelHandler?()
            }
            alert.addAction(cancelAction)
        }
        
        if hasTextField {
            alert.addTextField { (textField) in
            }
        }
        self.present(alertController: alert, from: controller)
    }
    
    private func present(alertController: UIAlertController, from controller: UIViewController?) {
        var presentingController: UIViewController?
        if let controller = controller {
            presentingController = controller
        } else {
            presentingController = UIApplication.shared.keyWindow?.rootViewController
        }
        presentingController?.present(alertController, animated: true, completion: nil)

    }
}

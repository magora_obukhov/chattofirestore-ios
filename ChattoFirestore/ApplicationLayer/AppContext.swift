import Foundation
import Firebase

protocol AlertDisplayingServiceContext {
    var alertDisplayingService: AlertDisplayingService { get set }
}

protocol FBAuthServiceContext {
    var fbAuthService: FBAuthService { get set }
}

protocol ChatServiceContext {
    var chatService: ChatService { get set }
}

typealias AppContext =
    AlertDisplayingServiceContext &
    FBAuthServiceContext &
    ChatServiceContext


open class AppContextImpl: AppContext {
    var alertDisplayingService: AlertDisplayingService
    var fbAuthService: FBAuthService
    var chatService: ChatService
    
    init() {
        self.alertDisplayingService = AlertControllerService()
        self.fbAuthService = FBAuthServiceImpl()
        self.chatService = FirestoreService()
    }
}

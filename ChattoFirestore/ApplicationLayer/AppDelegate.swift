import UIKit
import Firebase
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    var appContext: AppContext!
    private let disposeBag = DisposeBag()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let screenBounds = UIScreen.main.bounds
        self.window = UIWindow(frame: screenBounds)
        self.window?.makeKeyAndVisible()
        
        FirebaseApp.configure()
        
        self.appContext = AppContextImpl()
        self.appCoordinator = AppCoordinator(window: window!, context: appContext)
        self.appCoordinator.start()
        return true
    }
}


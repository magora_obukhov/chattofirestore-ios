import UIKit

class AppCoordinator: BaseCoordinator {
    
    //MARK: - Properties
    
    let window: UIWindow
    var context: AppContext

    
    //MARK: - Lifecycle
    
    init(window: UIWindow, context: AppContext) {
        self.window = window
        self.context = context
    }
    
    //MARK: - Flows
    
    override func start() {
        startRooms()
    }
    
    func startRooms() {
        Log("Start rooms")
        let nc = UINavigationController()
        window.changeRootViewController(nc)
        let childCoordinator = ChatRoomsCoordinator(router: nc, context: self.context)
        self.addDependency(childCoordinator)
        childCoordinator.start()
    }
}

import UIKit

extension UITableViewCell {
    func setBadgeText(_ text: String) {
        let accesoryBadge = UILabel()
        let fontSize: CGFloat = 16
        accesoryBadge.backgroundColor = .blue
        accesoryBadge.text = text
        accesoryBadge.textColor = .white
        accesoryBadge.font = UIFont.systemFont(ofSize: fontSize)
        accesoryBadge.textAlignment = NSTextAlignment.center
        accesoryBadge.clipsToBounds = true
        accesoryBadge.sizeToFit()

        var frame = accesoryBadge.frame
        frame.size.height += (0.4 * fontSize);
        frame.size.width += fontSize
        accesoryBadge.frame = frame
        accesoryBadge.layer.cornerRadius = frame.size.height/2.0;
        self.accessoryView = accesoryBadge
        self.accessoryType = .none;
    }
}

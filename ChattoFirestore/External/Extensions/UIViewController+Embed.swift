//
//  UIViewController+Embed.swift
//  EvolvePay
//


import Foundation
import UIKit

extension UIViewController {
    func configureChildViewController(_ childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChildViewController(childController)
        holderView?.addSubview(childController.view)
        constrainViewEqual(holderView: holderView!, view: childController.view)
        childController.didMove(toParentViewController: self)
    }
    
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
			toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
			toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        
        let pinLeading = NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal,
			toItem: holderView, attribute: .leading, multiplier: 1.0, constant: 0)
		
        let pinTrailing = NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal,
			toItem: holderView, attribute: .trailing, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeading, pinTrailing])
    }
}

extension UIView {
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    func addChildViewController(_ childController: UIViewController) {
        self.parentViewController?.configureChildViewController(childController, onView: self)
    }
    
}


extension UIViewController {
//    
//    func setCustomBackButton() {
//        if let count = self.navigationController?.viewControllers.count, count > 1 {
//            let backButton = UIBarButtonItem(image: Asset.back.image, style: .done, target: self, action: #selector(onBackButtonTap))
//            backButton.tintColor = .white
//            self.navigationItem.leftBarButtonItem = backButton
//        }
//    }
//    
    @objc
    func onBackButtonTap() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc
    func onCloseButtonTap() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
//
//extension UIViewController {
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
//    
//    func dismissKeyboard() {
//        view.endEditing(true)
//    }
//}


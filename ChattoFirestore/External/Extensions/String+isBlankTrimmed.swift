import Foundation




extension String {
    
    var isBlankTrimmed: Bool {
        let trimmed = self.trimmingCharacters(in: CharacterSet.whitespaces)
        return trimmed.isEmpty
    }
    
}

import Foundation
import UIKit

extension UINavigationController {
    
    func setTranslucent() {
        self.navigationBar.isTranslucent = true
        self.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.clear
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.backgroundColor = UIColor.clear
    }
    
    func setNotTranslucent() {
        self.navigationBar.isTranslucent = false
//        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.barTintColor = UIColor.clear
        self.navigationBar.shadowImage = nil
        self.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
    }

    
}

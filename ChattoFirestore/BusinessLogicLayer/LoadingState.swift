import Foundation


enum LoadingState {

	case notLoaded
	case loading
	case loadingFailed
	case loaded
	case reloading
	
}

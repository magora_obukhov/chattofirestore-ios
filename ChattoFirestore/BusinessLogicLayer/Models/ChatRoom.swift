import Foundation
import Firebase

struct ChatRoom {
    var id: String
    var name: String
    var owner: String
    var ref: DocumentReference?
    
    init(dto: ChatRoomDTO) {
        self.id = dto.id ?? ""
        self.name = dto.name ?? ""
        self.owner = dto.owner ?? ""
    }
}

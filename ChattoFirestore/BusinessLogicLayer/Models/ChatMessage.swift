import Foundation
import Firebase

protocol OwnedMessage {
    var senderID: String { get set }
    var senderName: String  { get set }
}

struct ChatMessage: OwnedMessage {
    
    var id: String
    var senderID: String
    var senderName: String
    var timeStamp: Date
    var text: String
    var documentSnapshot: DocumentSnapshot?
    
    init(dto: ChatMessageDTO) {
        self.id = dto.id ?? ""
        self.senderID = dto.senderID ?? ""
        self.senderName = dto.senderName ?? ""
        self.timeStamp = dto.timeStamp ?? Date(timeIntervalSinceNow: 0)
        self.text = dto.text ?? ""
    }
}

extension ChatMessage: Equatable {
    static func == (lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.id == rhs.id
    }
}

import Foundation
import RxSwift

enum ChatRoomsViewModelError: String, LocalizedError {
    case invalidRoomName = "com.chattoFirestore.chatRoomsViewModel.invalidRoomName"
    var errorDescription: String? { return NSLocalizedString(self.rawValue, comment: "") }
}

protocol ChatRoomsViewModel {
    var selectedRoom: PublishSubject<ChatRoom> { get }
    var rooms: Variable<[ChatRoom]> { get }
    var refreshRoomsList: Single<Void> { get }
    var loadingState: Variable<LoadingState> { get }
    var errorSignal: PublishSubject<Error> { get }
    func create(roomNamed: String?)
    func canEdit(chatRoom: ChatRoom) -> Bool
    func isOwnerForRoom(_ room: ChatRoom) -> Bool
}

class ChatRoomsViewModelImpl: ChatRoomsViewModel {
    let rooms = Variable<[ChatRoom]>([])
    let selectedRoom = PublishSubject<ChatRoom>()
    lazy var refreshRoomsList: Single<Void> = {
        return self.context.chatService.allRooms()
            .do(onSuccess: { [weak self] (rooms) in
                self?.loadingState.value = .loaded
            }, onError: { [weak self] (error) in
                self?.errorSignal.onNext(error)
                self?.loadingState.value = .loadingFailed
            }, onSubscribe: { [weak self] in
                self?.loadingState.value = .loading
            })
            .map({ (rooms) in
                return ()
            })
    }()
    
    let loadingState = Variable<LoadingState>(.notLoaded)
    var errorSignal = PublishSubject<Error>()
    
    private let context: AppContext
    private let disposeBag = DisposeBag()
    
    // MARK: - Lifecycle
    init(context: AppContext) {
        self.context = context
        self.loadingState.value = .loading
        self.login { [weak self] in
            guard let strongSelf = self else { return }
            self?.context.chatService.allRoomsObserver()
                .subscribe(onNext: { [weak self] (rooms) in
                    self?.rooms.value = rooms
                    self?.loadingState.value = .loaded
                    }, onError: { [weak self] (error) in
                        self?.errorSignal.onNext(error)
                        self?.loadingState.value = .loadingFailed
                })
                .disposed(by: strongSelf.disposeBag)
            
            self?.populateWithRooms()
        }
        
        

    }
    
    func create(roomNamed: String?) {
        guard let roomName = roomNamed, roomName.count > 0 else {
            self.errorSignal.onNext(ChatRoomsViewModelError.invalidRoomName)
            return
        }
        
        guard let user = self.context.fbAuthService.user else { return }
        self.context.chatService.create(roomNamed: roomName, owner: user)
            .subscribe(onSuccess: { (roomCreated) in
            
        }) { [weak self] (error) in
            print(error)
            self?.errorSignal.onNext(error)
        }
        .disposed(by: disposeBag)
    }
    
    func canEdit(chatRoom: ChatRoom) -> Bool {
        return self.isOwnerForRoom(chatRoom)
    }
    
    func isOwnerForRoom(_ chatRoom: ChatRoom) -> Bool {
        if let user = self.context.fbAuthService.user {
            return chatRoom.owner == user.uid
        }
        return false
    }
    
    private func login(completion: @escaping ()->()) {
        let email = "user2@mail.ru"
        let password = "123456"
        let nickname = "Daria"
        
        self.context.fbAuthService.signIn(email: email, password: password)
            .subscribe(onSuccess: { (user) in
                print("Success sign in")
                completion()
                
            }) { (error) in
                self.context.fbAuthService.signUp(email: email, password: password, nickname: nickname)
                    .subscribe(onCompleted: {
                        print("User created: ", email)
                        self.login(completion: completion)
                    }, onError: { (error) in
                        print("FATAL LOGIN ERROR: ", error)
                    })
                    .disposed(by: self.disposeBag)
            }
            .disposed(by: self.disposeBag)
    }
    
    private func populateWithRooms() {
        if let user = self.context.fbAuthService.user {
            let rooms = ["iOS Devs", "Android Devs"]
            for room in rooms {
                self.context.chatService.create(roomNamed: room, owner: user)
                    .subscribe(onSuccess: { (room) in
                        Log("Room created: \(room.name)")
                    })
                    .disposed(by: disposeBag)
            }
        }
    }
}

import UIKit
import RxSwift
import RxCocoa

class ChatRoomsViewController: UITableViewController {
    //MARK: - Properties
    var viewModel: ChatRoomsViewModel!
    var alertDisplayingService: AlertDisplayingService!
    private let disposeBag = DisposeBag()
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsSelectionDuringEditing = false
        self.addRefreshControl()
        self.addNewRoomButton()
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @objc func updateRooms() {
        self.viewModel.refreshRoomsList.subscribe({ [weak self] (_) in
            self?.tableView.reloadData()
        })
        .disposed(by: disposeBag)
    }
    
    //MARK: - Actions
    
    @objc func createNewRoomAction() {
        self.alertDisplayingService.showOkCancelInputAlert(title: "Create a room", message: "Enter room name",
                                                           okHandler: { [weak self] (roomName) in
                                                            self?.viewModel.create(roomNamed: roomName)
            }, cancelHandler: nil)


    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.rooms.value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = "RoomCell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if (cell == nil) {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        }
        let room = self.viewModel.rooms.value[indexPath.row]
        cell?.textLabel?.text = room.name
        if self.viewModel.isOwnerForRoom(room) {
            cell?.setBadgeText("Owned")
        }
        return cell!
    }

    // MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.selectedRoom.onNext(self.viewModel.rooms.value[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.viewModel.canEdit(chatRoom: self.viewModel.rooms.value[indexPath.row])
    }
    
    // MARK: - Private
    private func bindViewModel() {
        //subscribe To Rooms Changes
        self.refreshControl?.beginRefreshing()
        self.viewModel.rooms.asObservable()
            .subscribe(onNext: { [weak self] (RoomDTO) in
                self?.tableView.reloadData()
            })
            .disposed(by: disposeBag)
        
        //Subscribe to loading state
        self.viewModel.loadingState.asDriver()
            .drive(onNext: { [weak self] (loadingState) in
                if (loadingState == .loading || loadingState == .reloading) {
                    self?.refreshControl?.beginRefreshing()
                } else {
                    self?.refreshControl?.endRefreshing()
                }
            })
            .disposed(by: self.disposeBag)
        
        //Subscribe to errors
        self.viewModel.errorSignal
            .subscribe(onNext: { [weak self] (error) in
                self?.alertDisplayingService.showLocalizedError(error, okHandler: nil)
            })
            .disposed(by: self.disposeBag)
    }
    
    private func addRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(self.updateRooms), for: .valueChanged)
    }
    
    private func addNewRoomButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.createNewRoomAction))
    }
    
}

import Foundation
import RxSwift


class ChatRoomsCoordinator: BaseCoordinator {
    
    //MARK: - Properties
    
    let router: UINavigationController
    let context: AppContext
    private let disposeBag = DisposeBag()
    
    //MARK: - Lifecycle

    init(router: UINavigationController, context: AppContext) {
        self.router = router
        self.context = context
    }
    
    //MARK: - Flows
    
    override func start() {
        let vm = ChatRoomsViewModelImpl(context: self.context)
        let vc = ChatRoomsViewController(style: .plain)
        vc.viewModel = vm
        vc.alertDisplayingService = self.context.alertDisplayingService
        vm.selectedRoom
            .subscribe(onNext: { [weak self] (chatRoomToOpen) in
                self?.start(chatForRoom: chatRoomToOpen)
            })
            .disposed(by: disposeBag)
        router.pushViewController(vc, animated: false)
    }
    
    func start(chatForRoom: ChatRoom) {
        let childCoordinator = ChatMessagesCoordinator(router: self.router, context: self.context, chatRoom: chatForRoom)
        self.addDependency(childCoordinator)
        childCoordinator.completionHandler = { [weak self] in
            self?.removeDependency(childCoordinator)
        }
        childCoordinator.start()
    }
}

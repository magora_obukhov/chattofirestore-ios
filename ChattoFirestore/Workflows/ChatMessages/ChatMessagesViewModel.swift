import Foundation
import RxSwift

protocol ChatMessagesViewModel {
    var selfUID: String { get }
    func sendMessage(_ text: String) -> Single<ChatMessage>
}

class ChatMessagesViewModelImpl: ChatMessagesViewModel {
    
    var selfUID: String { return self.context.fbAuthService.user?.uid ?? ""}
    private let context: AppContext
    private let chatRoom: ChatRoom
    
    init(context: AppContext, chatRoom: ChatRoom) {
        self.context = context
        self.chatRoom = chatRoom
    }
    
    func sendMessage(_ text: String) -> Single<ChatMessage> {
        return self.context.chatService.create(message: text, chatRoom: self.chatRoom, user: self.context.fbAuthService.user!)
            .do(onError: { (error) in
                print(error)
            })
    }
}

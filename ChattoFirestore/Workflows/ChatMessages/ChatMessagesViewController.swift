import UIKit
import Chatto
import ChattoAdditions
//import FirebaseFirestoreUI

class ChatMessagesViewController: BaseChatViewController {
    var viewModel: ChatMessagesViewModel!
    var delegate: ChatDataSourceDelegateProtocol?
    var chatInputPresenter: BasicChatInputBarPresenter!
    var dataSource: ChatMessagesDataSource!
    var decorator: ChatMessagesDecorator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatDataSource = self.dataSource
        self.chatItemsDecorator = self.decorator
        self.title = "Chat"
    }
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        let textMessageBuilder = TextMessagePresenterBuilder(viewModelBuilder: TextBuilder(), interactionHandler: TextHandler())
        
        
        let photoMessageBuilder = PhotoMessagePresenterBuilder(viewModelBuilder: PhotoBuilder(), interactionHandler: PhotoHandler())
        
        textMessageBuilder.baseMessageStyle = Avatar()
        photoMessageBuilder.baseCellStyle = Avatar()
        
        return [
            TextModel.chatItemType : [textMessageBuilder],
            PhotoModel.chatItemType: [photoMessageBuilder]
        ]
    }
    
    //MARK: - Input bar
    override func createChatInputView() -> UIView {
        let chatInputView = ChatInputBar.loadNib()
        var appearance = ChatInputBarAppearance()
        appearance.sendButtonAppearance.title = NSLocalizedString("Send", comment: "")
        appearance.textInputAppearance.placeholderText = NSLocalizedString("Type a message", comment: "")
        self.chatInputPresenter = BasicChatInputBarPresenter(chatInputBar: chatInputView, chatInputItems: self.createChatInputItems(), chatInputBarAppearance: appearance)
        chatInputView.maxCharactersCount = 1000
        return chatInputView
    }
    
    func createChatInputItems() -> [ChatInputItemProtocol] {
        var items = [ChatInputItemProtocol]()
        items.append(self.createTextInputItem())
        return items
    }
    
    private func createTextInputItem() -> TextChatInputItem {
        let item = TextChatInputItem()
        item.textInputHandler = { text in
            //self?.dataSource.addTextMessage(text)
            let date = Date()
            let double = Double(date.timeIntervalSinceReferenceDate)
            let senderID = self.viewModel.selfUID
            let msgUID = ("\(double)" + senderID ).replacingOccurrences(of: ".", with: "")
            
            let message = MessageModel(
                uid: msgUID,
                senderId: senderID,
                type: TextModel.chatItemType,
                isIncoming: false,
                date: date,
                status: .sending
            )
            
            let textMessage = TextModel(messageModel: message, text: text)
            self.dataSource.addMessage(message: textMessage)
            
        }
        return item
    }
    

}


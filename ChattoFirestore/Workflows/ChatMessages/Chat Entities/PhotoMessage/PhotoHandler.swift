import Foundation
import Chatto
import ChattoAdditions

class PhotoHandler: BaseMessageInteractionHandlerProtocol {
    func userDidSelectMessage(viewModel: photoViewModel) {
        print("userDidSelectMessage")
    }
    
    func userDidDeselectMessage(viewModel: photoViewModel) {
        print("userDidDeselectMessage")
    }
    
    func userDidTapOnFailIcon(viewModel: photoViewModel, failIconView: UIView) {
        print("TapOnFail")
    }
    
    func userDidTapOnAvatar(viewModel: photoViewModel) {
        print("TapOnAvatar")
    }
    
    func userDidTapOnBubble(viewModel: photoViewModel) {
        print("TapOnBubble")
    }
    
    func userDidBeginLongPressOnBubble(viewModel: photoViewModel) {
        print("BeginLongPressOnBubble")
    }
    
    func userDidEndLongPressOnBubble(viewModel: photoViewModel) {
        print("EndLongPressOnBubble")
    }
}


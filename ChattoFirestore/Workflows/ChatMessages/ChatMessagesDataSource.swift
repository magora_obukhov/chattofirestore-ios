import Foundation
import Chatto

class ChatMessagesDataSource: ChatDataSourceProtocol {
    var chatItems = [ChatItemProtocol]()
    var delegate: ChatDataSourceDelegateProtocol?
    var viewModel: ChatMessagesViewModel!

    
    var hasMoreNext: Bool {
        return false
    }
    
    var hasMorePrevious: Bool {
        return false
    }
    
    func loadNext() {
        print(#function)
    }
    
    func loadPrevious() {
        print(#function)
    }
    
    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion: (Bool) -> Void) {
        print(#function)
    }
}

extension ChatMessagesDataSource {
    func addMessage(message: ChatItemProtocol) {
        self.chatItems.append(message)
        self.delegate?.chatDataSourceDidUpdate(self)
    }
}

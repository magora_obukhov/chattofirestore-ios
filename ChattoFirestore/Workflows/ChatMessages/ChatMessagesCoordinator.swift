import UIKit

class ChatMessagesCoordinator: BaseCoordinator {
    //MARK: - Properties
    
    let router: UINavigationController
    let context: AppContext
    
    private let chatRoom: ChatRoom
    
    //MARK: - Lifecycle
    
    init(router: UINavigationController, context: AppContext, chatRoom: ChatRoom) {
        self.router = router
        self.context = context
        self.chatRoom = chatRoom
    }
    
    override func start() {
        let vc = ChatMessagesViewController()
        let vm = ChatMessagesViewModelImpl(context: self.context, chatRoom: self.chatRoom)
        vc.viewModel = vm
        let dataSource = ChatMessagesDataSource()
        dataSource.viewModel = vm
        vc.dataSource = dataSource
        vc.decorator = ChatMessagesDecorator()
        self.router.pushViewController(vc, animated: true)
    }
}

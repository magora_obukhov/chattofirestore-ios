import Foundation
import ObjectMapper

struct ChatMessageDTO: Mappable {
    
    var id: String?
    var senderID: String?
    var senderName: String?
    var timeStamp: Date?
    var text: String?
    
    init(senderID: String, senderName: String?, text: String, timeStamp: Date = Date(timeIntervalSinceNow: 0)) {
        self.senderID = senderID
        self.senderName = senderName
        self.timeStamp = timeStamp
        self.text = text
    }
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        self.id <- map["id"]
        self.senderID <- map["senderID"]
        self.senderName <- map["senderName"]
        self.timeStamp <- (map["timeStamp"], DateTransform())
        self.text <- map["text"]
    }
}

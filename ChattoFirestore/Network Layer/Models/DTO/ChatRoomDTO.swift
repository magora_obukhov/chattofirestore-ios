import Foundation
import ObjectMapper

struct ChatRoomDTO: Mappable {
    
    var id: String?
    var name: String?
    var owner: String?
    
    init?(map: Map) {
    }
    
    init(id: String, name: String, owner: String) {
        self.id = id
        self.name = name
        self.owner = owner
    }
    
    mutating func mapping(map: Map) {
        self.name <- map["name"]
        self.owner <- map["owner"]
    }
}
